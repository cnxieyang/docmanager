<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="../common/comm_css.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'MyJsp.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script src="${pageContext.request.contextPath}/js/jquery.min.js?v=2.1.4"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.form.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/xco.js"></script>
   	<script type="text/javascript" src="${pageContext.request.contextPath}/js/public.js"></script>
   	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-xco-src.js"></script>
   	<script src="${pageContext.request.contextPath}/js/plugins/fancybox/jquery.fancybox.js"></script>
   	<SCRIPT src="${pageContext.request.contextPath}/js/xco.template.js" crossorigin="anonymous"></SCRIPT>
	<SCRIPT src="${pageContext.request.contextPath}/js/xco.databinding.js" crossorigin="anonymous"></SCRIPT>
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
			#cover{ 
				display:none; 
				position:fixed; 
				z-index:1; 
				top:0; 
				left:0; 
				width:100%; 
				height:100%; 
				background:rgba(0, 0, 0, 0.44); 
			} 
			#coverShow{ 
				display:none; 
				position:fixed; 
				z-index:2; 
				top:50%; 
				left:50%; 
				border:0px solid #127386; 
			} 
		</style>

  </head>
  
  <body style="text-align: center;">
  		 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal4">提交</button>
  		 <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">窗口标题</h4>
                        <small>这里可以显示副标题。
                    </div>
                    <div class="modal-body" style="padding:10px;">
							<textarea id="txt" rows="5" class="form-control" style="width:100%;resize: none;"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                        <button type="button" class="btn btn-primary" id="sub" data-dismiss="modal">保存</button>
                    </div>
                </div>
            </div>
        </div>
  		<table>
  			<thead>
  				<tr>
  					<th>参数名称</th>
  					<th>参数类型</th>
  				</tr>
  			</thead>
  			<tbody id="listgroup">
  			
  			</tbody>
  		</table>
  </body>
</html>
<script type="text/javascript">
	$("#sub").click(function(){
		var txt = $("#txt").val();
		var xco = new XCO();
		xco.fromXML(txt);
		var array = getKeyValue(xco);
		var html = '';
		for(var i = 0; i<array.length;i++){
			html+='<tr>';
			html+='	<td>'+array[i].name+'</td>';
			html+='	<td>'+array[i].type+'</td>';
			html+='</tr>';
		}
		$("#listgroup").html(html);
	})
	
	function getKeyValue(xco){
		var fields = xco.getFieldValueList();
		var array = new Array();
		for(var i = 0;i<fields.length;i++){
			var map ={
				name:fields[i].name,	
				type:null
			}
			if(fields[i] instanceof LongField){
				map.type='long';
			}
			if(fields[i] instanceof LongArrayField){
				map.type='longarray';
			}
			if(fields[i] instanceof XCOField){
				map.type='xco';
			}
			if(fields[i] instanceof XCOArrayField){
				map.type='xcoarray';
			}
			if(fields[i] instanceof XCOListField){
				map.type='xcolist';
			}
			if(fields[i] instanceof XCOSetField){
				map.type='xcoset';
			}
			//if(fields[i] instanceof ShortField){
			//	html+='		<td>short</td>';
			//}
			if(fields[i] instanceof IntegerField){
				map.type='int';
			}
			if(fields[i] instanceof IntegerArrayField){
				map.type='intarray';
			}
			if(fields[i] instanceof FloatField){
				map.type='float';
			}
			if(fields[i] instanceof FloatArrayField){
				map.type='floatarray';
			}
			if(fields[i] instanceof DoubleField){
				map.type='double';
			}
			if(fields[i] instanceof DoubleArrayField){
				map.type='doublearray';
			}
			//if(fields[i] instanceof CharField){
			//	html+='		<td>char</td>';
			//}
			if(fields[i] instanceof DateField){
				map.type='datetime';
			}
			if(fields[i] instanceof SqlDateField){
				map.type='date';
			}
			if(fields[i] instanceof SqlTimeField){
				map.type='time';
			}
			//if(fields[i] instanceof TimestampField){
			//	html+='		<td>timestamp</td>';
			//}
			if(fields[i] instanceof BigIntegerField){
				map.type='bigint';
			}
			if(fields[i] instanceof StringField){
				map.type='string';
			}
			if(fields[i] instanceof StringArrayField){
				map.type='stringarray';
			}
			if(fields[i] instanceof StringListField){
				map.type='stringlist';
			}
			if(fields[i] instanceof StringSetField){
				map.type='stringset';
			}
			array.push(map);
		}
		return array;
	}
</script>