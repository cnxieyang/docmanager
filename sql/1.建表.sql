use manager_db;
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS manager_interface;
CREATE TABLE manager_interface (
  interface_id			bigint(20) unsigned			NOT NULL	AUTO_INCREMENT	COMMENT '接口id',
  interface_num			varchar(50) 				NOT NULL	DEFAULT ''		COMMENT '接口编号',
  interface_name		varchar(50) 				NOT NULL	DEFAULT ''		COMMENT '接口名称',
  interface_url			varchar(50) 				NOT NULL	DEFAULT ''		COMMENT '接口Url',
  interface_type		tinyint(3) unsigned			NOT NULL	DEFAULT '0'		COMMENT '1-get 2-ajax post',
  interface_remark		varchar(2000)							DEFAULT ''		COMMENT '接口描述',
  img_url				varchar(50) 				NOT NULL	DEFAULT ''		COMMENT '相关图片Url',
  pro_id				bigint(20) unsigned			NOT NULL	DEFAULT '0'		COMMENT '项目id',
  mod_id				bigint(20) unsigned			NOT NULL	DEFAULT '0'		COMMENT '模块id',
  interface_param		text 						NOT NULL					COMMENT '接口参数',
  return_code			text						NOT NULL					COMMENT '接口返回code码',
  return_text			text 						NOT NULL					COMMENT '接口返回值（带格式）',
  state					tinyint(3) unsigned			NOT NULL	DEFAULT '0'		COMMENT '0-停用 1-正常',
  operator				varchar(50) 				NOT NULL	DEFAULT ''		COMMENT '创建人',
  create_time			datetime					NOT NULL					COMMENT '创建时间',
  update_time			datetime								DEFAULT NULL	COMMENT '修改时间',
  PRIMARY KEY (interface_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='接口信息表';

DROP TABLE IF EXISTS `manager_module`;
CREATE TABLE `manager_module` (
  `module_id`			bigint(20) unsigned			NOT NULL	AUTO_INCREMENT	COMMENT '模块id',
  `module_name`			varchar(50) 				NOT NULL	DEFAULT ''		COMMENT '模块名称',
  `pro_id`				bigint(20) unsigned			NOT NULL	DEFAULT '0'		COMMENT '项目id',
  `state`				tinyint(3) unsigned			NOT NULL	DEFAULT '0'		COMMENT '0-启用 1-禁用',
  `operator`			varchar(50) 				NOT NULL	DEFAULT ''		COMMENT '创建人',
  `create_time`			datetime					NOT NULL					COMMENT '创建时间',
  `update_time`			datetime								DEFAULT NULL	COMMENT '修改时间',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='模块信息表';

-- ----------------------------
-- Table structure for `program_info`
-- ----------------------------
DROP TABLE IF EXISTS `manager_project`;
CREATE TABLE `manager_project` (
  `id`				bigint(20) unsigned		NOT NULL AUTO_INCREMENT		COMMENT '项目id',
  `project_name`	varchar(50)				NOT NULL	DEFAULT ''		COMMENT '项目名',
  `project_remark`	varchar(2000) 						DEFAULT ''		COMMENT '项目描述',
  `in_explain`		varchar(2000) 						DEFAULT ''		COMMENT '接口说明',
  `operator`		varchar(50) 			NOT NULL	DEFAULT ''		COMMENT '创建人',
  `create_time`		datetime				NOT NULL					COMMENT '创建时间',
  `update_time`		datetime							DEFAULT NULL	COMMENT '修改时间',
  `state`			tinyint(3) unsigned		NOT NULL	DEFAULT '0'		COMMENT '0-启用 1-禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='项目信息表';


-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `manager_user`;
CREATE TABLE `manager_user` (
  `user_id`		bigint(20) unsigned		NOT NULL	 AUTO_INCREMENT		COMMENT '用户id',
  `user_name`	varchar(50) 			NOT NULL	DEFAULT ''			COMMENT '用户账户',
  `real_name`	varchar(50) 			NOT NULL	DEFAULT ''			COMMENT '用户名称',
  `password`	varchar(128) 			NOT NULL	DEFAULT ''			COMMENT '密码',
  `role_id`		bigint(20) unsigned		NOT NULL	DEFAULT '0'			COMMENT '角色ID',
  `create_time` datetime				NOT NULL						COMMENT '创建时间',
  `update_time` datetime							DEFAULT NULL		COMMENT '修改时间',
  `state`		tinyint(3) unsigned		NOT NULL	DEFAULT '0'			COMMENT '0-启用 1-禁用',
  `remark`		varchar(100) 						DEFAULT ''			COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='人员表';
